#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:d10b1a8ef504a35e9c68f47117bea1c535e8e3ba; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:0962660b8006f63b5b5cd7c7fea9c779659a9989 EMMC:/dev/block/platform/bootdevice/by-name/recovery d10b1a8ef504a35e9c68f47117bea1c535e8e3ba 33554432 0962660b8006f63b5b5cd7c7fea9c779659a9989:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
